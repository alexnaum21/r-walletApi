const info = require('../controllers').info;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the API!',
  }));

  app.post('/api/token', info.createToken);
  app.get('/api/token', info.listTokens);
  app.get('/api/tokens/:address', info.listMyTokens);
  app.get('/api/rates', info.getRates);
  app.delete('/api/token/:tokenId', info.destroyToken);

  app.all('/api/token', (req, res) => res.status(405).send({
    message: 'Method Not Allowed',
  }));
};
