const Token = require('../models').Token;
const Web3Helper = require('../services').web3Helper;
const RatesHelper = require('../services').ratesHelper;

module.exports = {
  createToken(req, res) {
    return Token
      .create({
        decimals: req.body.decimals,
        symbol: req.body.symbol,
        address: req.body.address
      })
      .then((token) => res.status(201).send(token))
      .catch((error) => res.status(400).send(error));
  },

  listTokens(req, res) {
    return Token
      .findAll({
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((tokens) => res.status(200).send(tokens))
      .catch((error) => res.status(400).send(error));
  },

  destroyToken(req, res) {
    return Token
      .findById(req.params.tokenId)
      .then(token => {
        if (!token) {
          return res.status(404).send({
            message: 'Token Not Found',
          });
        }
        return token
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  listMyTokens(req, res){
    let address = req.params.address;
    return Web3Helper
      .getTokensBalances(address)
      .then((balances) => res.status(200).json(balances))
      .catch((error) => res.status(400).json(error))
  },

  getRates(req, res){
    let currency = req.query.currency;
    return RatesHelper
      .getRates(currency)
      .then((rates) => res.status(200).json(rates))
      .catch((error) => res.status(400).json(error))
  }
};
