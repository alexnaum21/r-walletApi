const Token = require('../models').Token;
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];
const Web3 = require('web3');
const web3 = new Web3(config.web3Provider || "https://kovan.infura.io");
const erc20ABI = require('./erc20ABI');

const requestify = require('requestify');

const Web3Helper = {
  getTokensBalances: (address) => {
    return new Promise(function (fulfill, reject) {
      Web3Helper
        .getTransfersEvent(address)
        .then(Web3Helper.processAddresses)
        .then(Web3Helper.createTokens)
        .then(data => {
          Web3Helper
            .checkAddressBalances(address)
            .then(fulfill)
            .catch(reject)
        })
        .catch(reject);
    });
  },

  processAddresses: (addresses) => {
    return new Promise((fulfill, reject) => {

      let promises = addresses.map(addr => {
        return Web3Helper.getTokenInfo(addr);
      });

      Promise
        .all(promises)
        .then(fulfill)
        .catch(reject);
    });
  },

  getTransfersEvent: (address) => {
    return new Promise(function (fulfill, reject) {
      let url = 'http://api.etherscan.io/api?' +
        'module=logs' +
        '&action=getLogs' +
        '&apiKey=CU757U4KEENBRVU5BQI5UM5WFS3UWB7BSD' +
        '&fromBlock=0' +
        '&toBlock=latest' +
        '&topic0=0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef' +
        '&topic2=' + '0x000000000000000000000000' + address.substring(2);

      requestify.get(url, {timeout: 20000, headers: {connection: 'Close'}})
        .then(reqResult => {


          let response = reqResult.body ? JSON.parse(reqResult.body) : {};
          if (response.status == '1' && response.result) {
            if (!response.result.length) {
              return fulfill([]);
            } else {
              return fulfill(response.result.map(r => r.address));
            }
          } else {
            return fulfill([]);
          }
        }, err => {
          return reject({
            code: 500,
            message: err.message
          });
        })
        .catch(err => {
          return reject({
            code: 500,
            message: err.message
          });
        });
    });
  },

  getTokenInfo: (address) => {
    return new Promise(function (fulfill, reject) {
      let myContractInstance = new web3.eth.Contract(erc20ABI, address);
      let newTokenData = {address: address};

      // Promises, promises everywhere
      let p1 = myContractInstance.methods.name().call();
      let p2 = myContractInstance.methods.symbol().call();
      let p3 = myContractInstance.methods.decimals().call();

      // End of promises

      Promise
        .all([p1, p2, p3])
        .then(function (arrayOfResults) {
          newTokenData.name = arrayOfResults[0];
          newTokenData.symbol = arrayOfResults[1];
          newTokenData.decimals = arrayOfResults[2];

          return fulfill(newTokenData);
        })
        .catch(err => {
          console.log(err);
          return fulfill();
        });
    });
  },

  createTokens: (tokensData) => {
    return new Promise((fulfill, reject) => {
      let promises = tokensData.filter(data => data).map(data => {
        return new Promise((fulfill, reject) => {
          Token.findOne({
            where: {
              address: data.address
            }
          }).then(exist => {
            if (exist) return fulfill();
            else {
              console.log('insert', data)
              Token
                .create(data)
                .then(fulfill)
                .catch(reject);
            }
          })
            .catch(reject)
        });
      });

      Promise
        .all(promises)
        .then(fulfill)
        .catch(reject);
    });
  },

  checkAddressBalances: (address) => {
    return new Promise((fulfill, reject) => {
      Token
        .findAll({raw:true})
        .then(tokens => {
          let promises = tokens.map(t => {
            return Web3Helper.checkTokenBalance(t, address);
          });

          Promise
            .all(promises)
            .then(fulfill)
            .catch(reject);
        })
    });
  },

  checkTokenBalance: (tokenRaw, userAddress) => {
    return new Promise((fulfill, reject) => {
      let myContractInstance = new web3.eth.Contract(erc20ABI, tokenRaw.address);
      let tokenData = {
        address: tokenRaw.address,
        decimals: tokenRaw.decimals,
        name: tokenRaw.name,
        symbol: tokenRaw.symbol
      };

      myContractInstance
        .methods
        .balanceOf(userAddress)
        .call()
        .then(balance => {
          tokenData.balance = balance / Math.pow(10, tokenData.decimals);
          return fulfill(tokenData);
        })
        .catch(reject);
    });
  }
};

module.exports = Web3Helper;
