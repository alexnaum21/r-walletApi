const Coinbase = require('coinbase').Client;
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];
const requestify = require('requestify');
const coinbaseClient = new Coinbase({
  'apiKey': config.coinbaseKey,
  'apiSecret': config.coinbaseSecret
});

module.exports = {
  getRates: (currency = 'EUR') => {
    return new Promise((fulfill, reject) => {
      let url = 'https://api.coinbase.com/v2/exchange-rates?currency=' + currency;

      requestify.get(url, {timeout: 20000, headers: {connection: 'Close'}})
        .then(reqResult => {
          let response = reqResult.body ? JSON.parse(reqResult.body) : {};
          fulfill(response);
        }, err => {
          return reject({
            code: 500,
            message: err.message
          });
        })
        .catch(err => {
          return reject({
            code: 500,
            message: err.message
          });
        });
    });
  }
}